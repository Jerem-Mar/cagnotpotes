<?php

namespace App\Controller;

use App\Entity\Payment;
use App\Entity\Spending;
use App\Entity\SpendingType;
use App\Entity\Participant;
use App\Entity\Campaign;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SpendingController extends AbstractController
{
    /**
     * @Route("/spend", name="spend", methods="GET|POST")
     */
    public function spend(Request $request)
    {
        
        //Instancier la campaign
        
        

        //Enregistrer un participant
        $participant = new Participant();

        $campaign_id =  $request->request->get('campaign_id');

        $participant->setName($request->request->get('name'));
        $participant->setEmail($request->request->get('email'));

        $campaign = $this->getDoctrine()
                        ->getRepository(Campaign::class)
                        ->find($campaign_id);


        $participant->setCampaign($campaign);
        
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($participant);
        $em->flush();


        //Enregistrer le payment qui est dépendant du participant

        $spending = new spending();


        $amount = (int)$request->request->get("amount") * 100;
        $label = $request->request->get("content");

        $spending->setAmount($amount);
        $spending->setParticipant($participant);
        $spending->setLabel($label);

        $em = $this->getDoctrine()->getManager();
        $em->persist($spending);
        $em->flush();

        //Redirection vers la fiche campagne
        return $this->redirectToRoute('campaign_show', [
            "id" =>  $request->request->get('campaign_id')
        ]);


    }
}
